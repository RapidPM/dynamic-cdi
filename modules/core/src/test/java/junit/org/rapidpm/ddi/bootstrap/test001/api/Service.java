package junit.org.rapidpm.ddi.bootstrap.test001.api;

/**
 * Created by benjamin-bosch on 06.04.17.
 */
public interface Service {

  String doWork();

}
