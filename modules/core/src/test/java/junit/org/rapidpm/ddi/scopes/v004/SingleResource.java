package junit.org.rapidpm.ddi.scopes.v004;

public class SingleResource {

  public SingleResource() {
  }

  public final long value = Math.round(Math.random()*1000.0);
}
